#!/usr/bin/env sh

set -e

npm i
ls
echo 'end'
npm run build

mkdir -p build-web/build
cp -R dist  build-web/build
cp index.html  build-web/build

cp ci/Dockerfile build-web/Dockerfile
cp ci/default build-web/default
