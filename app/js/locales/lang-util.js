export const saveLangToLocalStorage = (lang) => {
	if (lang) {
		window.localStorage.setItem('tws-lang', lang)
	}
}
export const getLangFromLocalStorage = () => {
	const localStorageLang = window.localStorage.getItem('tws-lang')

	if (localStorageLang) {
		return localStorageLang
	}
	saveLangToLocalStorage(browserLanguage())

	return getLangFromLocalStorage()
}

const browserLanguage = () => {
	let language;
	if (navigator.appName == 'Netscape') {
		language = navigator.language;
	}
	else {
		language = navigator.browserLanguage;
	}

	if (language.indexOf('en') > -1){
		return "en"
	}
	else if (language === 'zh' || language === 'zh-CN'){
		return "zh"
	}
	else{
		return "zh_TW"
	}
}
