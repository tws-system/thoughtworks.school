import zh from './zh-Hans-CN.messages'
import en from './en.messages'
import zh_TW from './zh-TW.messages'

const locales = {
  zh: {
    intlLocale: 'zh-Hans',
    intlMessage: zh
  },
  zh_TW: {
    intlLocale: 'zh-TW',
    intlMessage: zh_TW
  },
	en: {
		intlLocale: 'en',
		intlMessage: en
	}
}

export default locales
