import "./imports";
import locale from './locales'
import {saveLangToLocalStorage, getLangFromLocalStorage} from './locales/lang-util'

(function ($) {
	"use strict";
	if (isIE()) {
		alert('请使用 Chrome 浏览器以获得更好的体验')
	}
	// Custom
	const stickyToggle = function (sticky) {
		const scrollElement = $(window);
		if (scrollElement.scrollTop() >= 57) {
			sticky.addClass("bg-inverse");
		} else {
			sticky.removeClass("bg-inverse");
		}
	};

	const hiddenToggle = function (element) {
		const scrollElement = $(window);
		if (scrollElement.scrollTop() >= 195) {
			element.removeClass("hidden");
		} else {
			element.addClass("hidden");
		}
	};

	// Scroll & resize events
	$(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
		stickyToggle($('#navbar'));
		hiddenToggle($('#navbar-logo'));

	});

	// On page load
	stickyToggle($('#navbar'));
	hiddenToggle($('#navbar-logo'))


	// on page ready ： language switch
	$(document).ready(function () {
		onListenLocaleSelectChange()
		initLanguage()
	})

	function onListenLocaleSelectChange() {
		$("#locale-select").change(function () {
			const lang = $(this).val()
			saveLangToLocalStorage(lang)
			parserLanguage()
		})
	}

	function initLanguage() {
		const lang = getLangFromLocalStorage()
		$("#locale-select").val(lang)
		parserLanguage()
	}

	function parserLanguage() {
		const lang = getLangFromLocalStorage()
		const initMessage = locale[lang].intlMessage
		$("[tws-lang]").each(function (index, element) {
			const text = $(element).attr("tws-lang")
			if (!initMessage[text]) {
				console.warn(text, ' is not exist')
			}
			$(element).text(initMessage[text])
		})
	}

})(jQuery);

function isIE() {
	return !!window.ActiveXObject || "ActiveXObject" in window;
}
